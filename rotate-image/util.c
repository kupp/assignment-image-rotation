#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include "util.h"

_Noreturn void err( const char* msg, ... ) {
  va_list args;
  va_start (args, msg);
  vfprintf(stderr, msg, args);
  va_end (args);
  exit(1);
}

void log_save_file_status(enum save_file_status status) {
    static char const* const MESSAGES[] = {
            [SAVE_OK] = "Successfully saved!",
            [SAVE_FAIL] = "Saved failed!"
    };

    if (status == SAVE_OK) {
       printf("%s\n", MESSAGES[status]);
    } else {
        err("%s\n", MESSAGES[status]);
    }
}

void log_open_file_status(enum open_file_status status) {
    static char const* const MESSAGES[] = {
            [OPEN_OK] = "Successfully opened!",
            [OPEN_FAIL] = "Failed on open!",
            [OPENED_BUT_READ_FAIL] = "File open, but read failed!"
    };

    if (status == OPEN_OK) {
        printf("%s\n", MESSAGES[status]);
    } else {
        err("%s\n", MESSAGES[status]);
    }
}

void log_write_status(enum write_status status) {
    static char const* const MESSAGES[] = {
            [WRITE_OK] = "Successfully written!",
            [WRITE_ERROR] = "Fail on writing a file!"
    };

    if (status == WRITE_OK) {
        printf("%s\n", MESSAGES[status]);
    } else {
        err("%s\n", MESSAGES[status]);
    }
}

void log_read_status(enum read_status status) {
    static char const* const MESSAGES[] = {
            [READ_OK] = "Successfully read!",
            [READ_INVALID_SIGNATURE] = "Invalid signature!",
            [READ_INVALID_BITS] = "Invalid bits!",
            [READ_INVALID_HEADER] = "Invalid header!",
            [READ_FILE_ERROR] = "Read file error!"
    };

    if (status == READ_OK) {
        printf("%s\n", MESSAGES[status]);
    } else {
        err("%s\n", MESSAGES[status]);
    }
}
