#include <stdlib.h>
#include "image.h"

struct image rotate( struct image const source ) {
    struct pixel* rotated_data = malloc(sizeof(struct pixel) * source.width * source.height);
    struct image rotated_image = {.width = source.height, .height = source.width, .data = rotated_data};

    for (uint64_t i = 0; i < source.height; i++) {
        for (size_t j = 0; j < source.width; j++) {
            rotated_data[j * source.height + (source.height - 1 - i)] = source.data[i * source.width + j];
        }
    }

    return rotated_image;

}