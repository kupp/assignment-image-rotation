#include "bmp.h"

#include <inttypes.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include "image.h"
#include "util.h"

#define PRI_SPECIFIER(e) (_Generic( (e), uint16_t : "%" PRIu16, uint32_t: "%" PRIu32, default: "NOT IMPLEMENTED" ))

#define PRINT_FIELD( t, name ) \
    fprintf( f, "%-17s: ",  # name ); \
    fprintf( f, PRI_SPECIFIER( header-> name ) , header-> name );\
    fprintf( f, "\n");

#define SIGNATURE 0x4d42
#define PADDING_BYTE_VALUE 0
#define CALC_PADDING(width) ((4 - (width * 3 % 4)) % 4)

static struct bmp_header new_bmp_header(uint32_t width, uint32_t height) {
    return (struct bmp_header) {
            .bfType = SIGNATURE,
            .bfileSize = width * height * 24,
            .bfReserved = 0,
            .bOffBits = 54,
            .biSize = 40,
            .biWidth = width,
            .biHeight = height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = sizeof(struct bmp_header) + width * height * 24,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
}

/*static void bmp_header_print( struct bmp_header const* header, FILE* f ) {
   FOR_BMP_HEADER( PRINT_FIELD )
}*/

static bool read_header( FILE* f, struct bmp_header* header ) {
    return fread( header, sizeof( struct bmp_header ), 1, f );
}

/*static bool read_header_from_file( const char* filename, struct bmp_header* header ) {
    if (!filename) return false;
    FILE* f = fopen( filename, "rb" ); 
    if (!f) return false;
    if (read_header( f, header ) ) {
        fclose( f );
        return true; 
    }

    fclose( f );
    return false;
}*/

/*
 Было бы не совсем логично, если бы эта функция возвращала enum read_status.
 Но члены перечисления header_status должны пересекаться с read_status. По-этому
 все константы в header_status соответствуют константам в read_status.Это позволит
 не проверять каждую ошибку хидера отдельно и на её основе возвращать соответствующий
 read_status.
*/
enum header_status read_check_header(FILE* in, struct bmp_header* header) {
    if (!read_header(in, header)) {
        return INVALID_HEADER;
    }
    if (header->bfType != SIGNATURE) {
        return HEADER_INVALID_SIGNATURE;
    }
    return HEADER_OK;
}

enum read_status from_bmp(FILE* in, struct image* img) {
    struct bmp_header header = { 0 };
    enum header_status header_stat = read_check_header(in, &header);
    if (header_stat != HEADER_OK) {
        //абсолютно легитимно, так как константы соответсвуют (логически header_status подмножество read_status)
        return (enum read_status)header_stat;
    }

    img->width = header.biWidth;
    img->height = header.biHeight;

    img->data = malloc(sizeof(struct pixel) * header.biHeight * header.biWidth);

    size_t const PADDING = CALC_PADDING(header.biWidth);
    for (size_t i = 0; i < header.biHeight; i++) {
        size_t status = fread(&img->data[i * header.biWidth], sizeof(struct pixel), header.biWidth, in);
        if (status < 1) {
            return READ_FILE_ERROR;
        }
        fseek(in, PADDING, SEEK_CUR);
    }

    return READ_OK;
}

enum write_status to_bmp(FILE* out, struct image const* img) {
    size_t const PADDING = CALC_PADDING(img->width);
    char const PADDING_BYTE = PADDING_BYTE_VALUE;

    struct bmp_header header = new_bmp_header(img->width, img->height);
    size_t status = fwrite(&header, sizeof(struct bmp_header), 1, out);
    if (status < 1) {
        return WRITE_ERROR;
    }

    for (size_t i = 0; i < img->height; i++) {
        status = fwrite(&img->data[i * img->width], sizeof(struct pixel), img->width, out);
        if (status < 1) {
            return WRITE_ERROR;
        }
        fwrite(&PADDING_BYTE, 1, PADDING, out);
    }

    return WRITE_OK;
}

enum open_file_status from_bmp_path(char const* path, struct image* image) {
    FILE* in = fopen(path, "rb");
    if (!in) {
        return OPEN_FAIL;
    }

    enum read_status status = from_bmp(in, image);
    fclose(in);
    log_read_status(status);
    if (status != READ_OK) {
        return OPENED_BUT_READ_FAIL;
    } else {
        return OPEN_OK;
    }
}

enum save_file_status to_bmp_path(char const* path, struct image* image) {
    FILE* out = fopen(path, "wb");
    if (!out) {
        return SAVE_FAIL;
    }

    enum write_status status = to_bmp(out, image);
    fclose(out);
    log_write_status(status);
    if (status != WRITE_OK) {
        return SAVE_FAIL;
    } else {
        return SAVE_OK;
    }
}
