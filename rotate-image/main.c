#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "bmp.h"
#include "util.h"
#include "image.h"


void usage() {
    fprintf(stderr, "Usage: ./rotate-image [source] [destination]\n");
}

int main( int argc, char** argv ) {
    if (argc != 3) usage();
    if (argc < 3) err("Not enough arguments \n" );
    if (argc > 3) err("Too many arguments \n" );

    struct image* img = malloc(sizeof (struct image));
    puts("Trying to open source file...");
    log_open_file_status(from_bmp_path(argv[1], img));
    puts("Trying to rotate image...");
    struct image rotated_img = rotate(*img);
    puts("Successfully rotated!");
    puts("Trying to save rotated image...");
    log_save_file_status(to_bmp_path(argv[2], &rotated_img));

    return 0;
}
