#ifndef _UTIL_H_
#define _UTIL_H_

enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE = 1,
    READ_INVALID_BITS = 2,
    READ_INVALID_HEADER,
    READ_FILE_ERROR
};

enum write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum open_file_status {
    OPEN_OK =0,
    OPEN_FAIL,
    OPENED_BUT_READ_FAIL,
};

enum save_file_status {
    SAVE_OK =0,
    SAVE_FAIL,
};

enum header_status {
    HEADER_OK = 0,
    HEADER_INVALID_SIGNATURE = 1,
    INVALID_HEADER = 2,
};

_Noreturn void err( const char* msg, ... );

void log_read_status(enum read_status status);
void log_write_status(enum write_status status);
void log_save_file_status(enum save_file_status status);
void log_open_file_status(enum open_file_status status);
#endif
